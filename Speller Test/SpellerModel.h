//
//  SpellerModel.h
//  Speller Test
//
//  Created by Alexander on 06.10.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SpellerModel : NSObject

@property (nonatomic) NSMutableDictionary *currentUser;

+ (SpellerModel *)sharedInstance;
- (void)getMessageStatisticsForCurrentUserWithCompletionBlock:(void (^)(NSInteger statistics, NSArray *mostFrequent))callback;
- (void)getMessageStatisticsForFriendWithCompletionBlock:(void (^)(NSArray *friendsData))callback;

@end
