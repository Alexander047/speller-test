//
//  TestYourselfViewController.h
//  Speller Test
//
//  Created by Alexander on 06.10.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestYourselfViewController : UITableViewController

- (void)reloadProfileCell;
- (void)getFriendsInfo;

@end
