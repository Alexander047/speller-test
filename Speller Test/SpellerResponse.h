//
//  SpellerResponse.h
//  Speller Test
//
//  Created by Alexander on 06.10.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SpellerResponse : NSObject

@property (nonatomic) NSInteger code;
@property (nonatomic) NSInteger row;
@property (nonatomic) NSInteger column;
@property (nonatomic) NSInteger position;
@property (nonatomic) NSInteger length;
@property (retain, nonatomic) NSArray *suggestions;
@property (retain, nonatomic) NSString *word;

@end
