//
//  TestYourselfViewController.m
//  Speller Test
//
//  Created by Alexander on 06.10.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import "TestYourselfViewController.h"
#import <AFNetworking/AFNetworking.h>
#import "SpellerResponseParser.h"
#import "SpellerResponse.h"
#import "SpellerModel.h"
#import <VKSdk.h>
#import "ProfileTableViewCell.h"
#import "FriendTableViewCell.h"

@interface TestYourselfViewController () <VKSdkDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) ProfileTableViewCell *profileCell;
@property (strong, nonatomic) NSArray <FriendTableViewCell *> *friendsCells;
@property (strong, nonatomic) NSArray *friendsData;

@property (strong, nonatomic) UIView *hoverView;

@end

@implementation TestYourselfViewController {
    NSArray <SpellerResponse *> *temporaryArray;
}

@synthesize friendsCells, profileCell, hoverView, activityIndicator;

- (void)viewDidLoad {
    [super viewDidLoad];
    profileCell = [[NSBundle mainBundle] loadNibNamed:@"ProfileTableViewCell" owner:self options:nil].lastObject;
    profileCell.parentVc = self;
    [profileCell layoutIfNeeded];
    friendsCells = @[[[NSBundle mainBundle] loadNibNamed:@"FriendTableViewCell" owner:self options:nil].lastObject, [[NSBundle mainBundle] loadNibNamed:@"FriendTableViewCell" owner:self options:nil].lastObject, [[NSBundle mainBundle] loadNibNamed:@"FriendTableViewCell" owner:self options:nil].lastObject, [[NSBundle mainBundle] loadNibNamed:@"FriendTableViewCell" owner:self options:nil].lastObject, [[NSBundle mainBundle] loadNibNamed:@"FriendTableViewCell" owner:self options:nil].lastObject, [[NSBundle mainBundle] loadNibNamed:@"FriendTableViewCell" owner:self options:nil].lastObject, [[NSBundle mainBundle] loadNibNamed:@"FriendTableViewCell" owner:self options:nil].lastObject, [[NSBundle mainBundle] loadNibNamed:@"FriendTableViewCell" owner:self options:nil].lastObject, [[NSBundle mainBundle] loadNibNamed:@"FriendTableViewCell" owner:self options:nil].lastObject, [[NSBundle mainBundle] loadNibNamed:@"FriendTableViewCell" owner:self options:nil].lastObject];
    [self initGui];
    [self initVkSDK];
}

- (void)reloadProfileCell{
    [self.tableView reloadData];
}

- (UIView *)hoverView {
    if (!hoverView) {
        hoverView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        [hoverView setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7f]];
        hoverView.alpha = 0;
    } return hoverView;
}

- (void)getFriendsInfo {
    [self.navigationController.view addSubview:hoverView];
    [hoverView addSubview:activityIndicator];
    [activityIndicator setCenter:hoverView.center];
    [activityIndicator startAnimating];
    [UIView animateWithDuration:0.3 animations:^{
        hoverView.alpha = 1.0f;
    }];
    [[SpellerModel sharedInstance] getMessageStatisticsForFriendWithCompletionBlock:^(NSArray *friendsData) {
        [self reloadTableWithData:friendsData];
    }];
}

- (void)reloadTableWithData:(NSArray *)data {
    self.friendsData = data;
    [self.tableView reloadData];
    [UIView animateWithDuration:0.3 animations:^{
        hoverView.alpha = 0;
        [activityIndicator stopAnimating];
        [activityIndicator removeFromSuperview];
    }completion:^(BOOL finished) {
        [hoverView removeFromSuperview];
    }];
}

- (void)viewDidAppear:(BOOL)animated {
    [[UIApplication sharedApplication].keyWindow addSubview:self.hoverView];
    [self.hoverView setFrame:[UIScreen mainScreen].bounds];
}

- (void)initGui {
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
}

- (void)initVkSDK {
    [VKSdk initializeWithDelegate:self andAppId:@"5095461"];
    if (![VKSdk wakeUpSession]) {
        [self initAuthAlert];
    } else {
        [self getUserInfo];
    }
}

- (void)initAuthAlert {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Авторизация" message:@"Для данной операции Вам необходимо пройти авторизацию через Вконтакте" delegate:self cancelButtonTitle:@"Нет, спасибо" otherButtonTitles:@"Авторизоваться", nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case 1:
            [VKSdk authorize:@[VK_PER_FRIENDS, VK_PER_WALL, VK_PER_MESSAGES]];
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate & Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (!self.friendsData) {
        return 1;
    }
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 1) {
        return 10;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 32.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        CGFloat cellContentHeight = [profileCell getCurrentCellHeight];
        CGFloat screenHeight = [UIScreen mainScreen].applicationFrame.size.height - 44.0f - 32.0f;
        if (cellContentHeight <= screenHeight && !self.friendsData) {
            return screenHeight;
        }
        return cellContentHeight;
    }
    return 60.0f;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return @"Профиль";
    } else if (section == 1) {
        return @"Друзья";
    }
    return @"";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        FriendTableViewCell *cell = friendsCells[indexPath.row];
        cell.userStatistics = [self.friendsData[indexPath.row][@"mistakes"] floatValue];
        [cell loadUser:(NSDictionary *)self.friendsData[indexPath.row][@"user_id"]];
        return cell;
    }
    return profileCell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark - VK Methods
- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError {
    
}

- (void)vkSdkReceivedNewToken:(VKAccessToken *)newToken {
    [self getUserInfo];
}

- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken {
    
}

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller {
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)vkSdkUserDeniedAccess:(VKError *)authorizationError {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)logOut:(id)sender {
    [VKSdk forceLogout];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getUserInfo {
    VKRequest *getUser = [[VKApi users] get:@{VK_API_FIELDS : @"id,first_name,last_name,photo_200,photo_100"}];
    [getUser setPreferredLang:@"ru"];
    [getUser executeWithResultBlock:^(VKResponse * response) {
        NSLog(@"Json result: %@", response.json);
        [SpellerModel sharedInstance].currentUser = response.json[0];
        self.navigationItem.rightBarButtonItem.enabled = YES;
        [profileCell loadUserAvatar];
    } errorBlock:^(NSError * error) {
        if (error.code != VK_API_ERROR) {
            [error.vkError.request repeat];
        } else {
            NSLog(@"VK error: %@", error);
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
