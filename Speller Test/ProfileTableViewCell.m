//
//  ProfileTableViewCell.m
//  Speller Test
//
//  Created by Alexander on 09.10.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import "ProfileTableViewCell.h"
#import "SpellerModel.h"

@implementation ProfileTableViewCell

@synthesize statisticView;

- (void)awakeFromNib {
    [self initGui];
}

- (void)initGui {
    self.userAvatar.layer.cornerRadius = [[UIScreen mainScreen] bounds].size.width / 4.0f;
    self.userAvatar.layer.masksToBounds = YES;
    self.testButton.layer.cornerRadius = 4.0f;
    self.testButton.layer.masksToBounds = YES;
    self.testFriendsButton.layer.cornerRadius = 4.0f;
    self.testFriendsButton.layer.masksToBounds = YES;
}

- (UIView *)statisticView {
    if (!statisticView) {
        statisticView = [[UIView alloc] init];
        statisticView.backgroundColor = [UIColor colorWithRed:1 green:0 blue:0 alpha:1];
        [self.contentView addSubview:statisticView];
    } return statisticView;
}

- (IBAction)testFriends:(id)sender {
    [UIView animateWithDuration:0.3 animations:^{
        self.testFriendsButton.alpha = 0;
    } completion:^(BOOL finished) {
        self.testFriendsButton.hidden = YES;
        [self.parentVc getFriendsInfo];
    }];
}

- (IBAction)testMyself:(id)sender {
    [UIView animateWithDuration:0.3 animations:^{
        self.testButton.alpha = 0;
    } completion:^(BOOL finished) {
        self.testButton.hidden = YES;
    }];
    [self.statisticView setFrame:CGRectMake(20.0f, self.testButton.frame.origin.y, 0, 25.0f)];
    [[SpellerModel sharedInstance] getMessageStatisticsForCurrentUserWithCompletionBlock:^(NSInteger value, NSArray *mostFrequent) {
        [UIView animateWithDuration:0.3 animations:^{
        }completion:^(BOOL finished) {
            [UIView animateWithDuration:3.0f animations:^{
                [self.statisticView setBackgroundColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:1]];
                [self.statisticView setFrame:CGRectMake(20.0f, self.testButton.frame.origin.y, ((100.0f - (CGFloat)value) / 100.0f) * (self.contentView.frame.size.width - 40.0f), 25.0f)];
                [self.statisticView setBackgroundColor:[UIColor colorWithRed:1.0f - ((100.0f - (CGFloat)value) / 100.0f) green:((100.0f - (CGFloat)value) / 100.0f) blue:0 alpha:1.0f]];
            }completion:^(BOOL finished) {
                self.cheerLabel.text = [NSString stringWithFormat:@"Проверено 100 последних сообщений, найдено %ld ошибок.\nНаиболее частые ошибки: \"%@\", \"%@\", \"%@\".\nИспользуйте меньше сленга, и Ваш результат улучшится!", (long)value, mostFrequent[mostFrequent.count - 1], mostFrequent[mostFrequent.count - 2], mostFrequent[mostFrequent.count - 3]];
                self.cheerLabel.hidden = NO;
                self.testFriendsButton.hidden = NO;
                [self layoutIfNeeded];
                [self.parentVc reloadProfileCell];
                
            }];
        }];
    }];
}

- (void)loadUserAvatar {
    self.nameLabel.text = [NSString stringWithFormat:@"%@ %@", [SpellerModel sharedInstance].currentUser[@"first_name"], [SpellerModel sharedInstance].currentUser[@"last_name"]];
    self.testButton.hidden = NO;
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[SpellerModel sharedInstance].currentUser[@"photo_200"]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:15];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if (data) {
            self.userAvatar.image = [UIImage imageWithData:data];
        }
        else NSLog(@"error while pulling image");
    }];
}

- (CGFloat)getCurrentCellHeight {
    if (self.cheerLabel.hidden == YES) {
        return CGRectGetMaxY(self.testButton.frame) + 16.0f;
    } else {
        if (self.testFriendsButton.hidden == YES) {
            return CGRectGetMaxY(self.cheerLabel.frame) + 16.0f;
        } else {
            return CGRectGetMaxY(self.testFriendsButton.frame) + 16.0f;
        }
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
