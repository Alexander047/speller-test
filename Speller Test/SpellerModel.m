//
//  SpellerModel.m
//  Speller Test
//
//  Created by Alexander on 06.10.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import "SpellerModel.h"
#import <AFNetworking/AFNetworking.h>
#import "SpellerResponseParser.h"
#import "SpellerResponse.h"
#import <VKSdk.h>

@implementation SpellerModel {
    NSInteger checkedFirendsCounter;
}

@synthesize currentUser;

+ (SpellerModel *)sharedInstance {
    static SpellerModel *model = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        model = [[self alloc] init];
    });
    return model;
}

- (NSMutableDictionary *)currentUser {
    if (!currentUser) {
        currentUser = [[NSMutableDictionary alloc] init];
    }
    return currentUser;
}

- (void)getMessageStatisticsForCurrentUserWithCompletionBlock:(void (^)(NSInteger, NSArray *))callback {
    NSArray *messagesArray;
        messagesArray = [self getMyMessages:callback];
}

- (void)getMessageStatisticsForFriendWithCompletionBlock:(void (^)(NSArray *))callback {
    NSMutableArray *array = [NSMutableArray new];
    VKRequest * getRequest = [VKRequest requestWithMethod:@"friends.get" andParameters:@{@"order" : @"hints", @"count" : @10, @"fields" : @"photo_100"} andHttpMethod:@"GET"];
    [getRequest executeWithResultBlock:^(VKResponse * response) {
        for (NSDictionary *dic in response.json[@"items"]) {
            [array addObject:[NSMutableDictionary dictionaryWithDictionary:dic]];
        }
        [self getMessagesForFriends:array completionBlock:callback];
    } errorBlock:^(NSError * error) {
        if (error.code != VK_API_ERROR) {
            [error.vkError.request repeat];
        }
        else {
            NSLog(@"VK error: %@", error);
        }
    }];
}

- (void)getMessagesForFriends:(NSMutableArray *)friends completionBlock:(void (^)(NSArray *))callback {
    NSMutableArray *requests = [NSMutableArray new];
    for (NSMutableDictionary *friend in friends) {
        VKRequest * getMessages = [VKRequest requestWithMethod:@"messages.getHistory" andParameters:@{@"user_id" : friend[@"id"], @"count" : @200} andHttpMethod:@"GET"];
        [requests addObject:getMessages];
    }
    VKBatchRequest * batch = [[VKBatchRequest alloc] initWithRequestsArray:requests];
    [batch executeWithResultBlock:^(NSArray *responses) {
        NSMutableArray *dialogs = [NSMutableArray new];
        for (NSInteger y = 0; y < 10; y++) {
            VKResponse *resp = responses[y];
            NSArray *messages = resp.json[@"items"];
            NSMutableArray *messageBodys = [NSMutableArray new];
            for (NSInteger x = messages.count - 1; x >= 0; x--) {
                if ([messages[x][@"user_id"] integerValue] != [messages[x][@"from_id"] integerValue]) {
                    [messageBodys addObject:(NSString *)messages[x][@"body"]];
                }
            }
            [dialogs addObject:@{@"messages" : messageBodys, @"user_id" : friends[y]}];
        }
        [self getStatisticsForDialogs:dialogs completionBlock:callback];
    } errorBlock:^(NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)getStatisticsForDialogs:(NSArray *)dialogs completionBlock:(void (^)(NSArray *))callback {
    NSMutableArray *results = [NSMutableArray new];
    for (NSArray *messages in dialogs) {
        [self getStatisticsForMessagesWithFloatCallback:[(NSDictionary *)messages valueForKey:@"messages"] callback:^(CGFloat mistakes) {
            [results addObject:@{@"mistakes" : [NSNumber numberWithFloat:mistakes], @"user_id" : [(NSDictionary *)messages valueForKey:@"user_id"]}];
            if (results.count == 10 && callback) {
                callback(results);
            }
        }];
    }
}

- (NSArray *)getMyMessages:(void (^)(NSInteger, NSArray *))callback {
    NSMutableArray *array = [NSMutableArray new];
    VKRequest * getRequest = [VKRequest requestWithMethod:@"messages.get" andParameters:@{@"out" : @1, @"count" : @100} andHttpMethod:@"GET"];
    [getRequest executeWithResultBlock:^(VKResponse * response) {
        for (NSDictionary *dic in response.json[@"items"]) {
            [array addObject:dic[@"body"]];
        }
        [self getStatisticsForMessages:array callback:callback];
    } errorBlock:^(NSError * error) {
        if (error.code != VK_API_ERROR) {
            [error.vkError.request repeat];
        }
        else {
            NSLog(@"VK error: %@", error);
        }
    }];
    return array;
}

- (void)getStatisticsForMessages:(NSArray *)messages callback:(void (^)(NSInteger, NSArray *))callback {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSData *data = [NSJSONSerialization dataWithJSONObject:messages options:NSJSONWritingPrettyPrinted error:nil];
    NSString *json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSDictionary *parameters = @{@"text": json};
    [manager POST:@"https://speller.yandex.net/services/spellservice.json/checkTexts" parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSMutableArray <SpellerResponse *> *array = [[NSMutableArray alloc] initWithArray:[SpellerResponseParser parsedResponse:responseObject[0]]];
        NSMutableDictionary <NSString*, NSNumber *> *frequency = [NSMutableDictionary new];
        for (NSInteger x = array.count - 1; x >= 0; x--) {
            SpellerResponse *pResp = array[x];
            if ([[pResp.word substringToIndex:1] isEqualToString:@"n"]) {
                [array removeObject:pResp];
            } else {
                if (frequency[pResp.word]) {
                    NSInteger y = [frequency[pResp.word] integerValue];
                    y++;
                    frequency[pResp.word] = [NSNumber numberWithInteger:y];
                } else {
                    [frequency setValue:[NSNumber numberWithInteger:1] forKey:pResp.word];
                }
            }
        }
        if (callback) {
            callback(array.count, [self getSortedForDic:frequency]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)getStatisticsForMessagesWithFloatCallback:(NSArray *)messages callback:(void (^)(CGFloat mistakes))callback {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSData *data = [NSJSONSerialization dataWithJSONObject:messages options:NSJSONWritingPrettyPrinted error:nil];
    NSString *json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSDictionary *parameters = @{@"text": json};
    [manager POST:@"https://speller.yandex.net/services/spellservice.json/checkTexts" parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSMutableArray <SpellerResponse *> *array = [[NSMutableArray alloc] initWithArray:[SpellerResponseParser parsedResponse:responseObject[0]]];
        NSMutableDictionary <NSString*, NSNumber *> *frequency = [NSMutableDictionary new];
        for (NSInteger x = array.count - 1; x >= 0; x--) {
            SpellerResponse *pResp = array[x];
            if ([[pResp.word substringToIndex:1] isEqualToString:@"n"]) {
                [array removeObject:pResp];
            } else {
                if (frequency[pResp.word]) {
                    NSInteger y = [frequency[pResp.word] integerValue];
                    y++;
                    frequency[pResp.word] = [NSNumber numberWithInteger:y];
                } else {
                    [frequency setValue:[NSNumber numberWithInteger:1] forKey:pResp.word];
                }
            }
        }
        if (callback) {
            callback(1.0f - (CGFloat)array.count / (CGFloat)[messages count]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (NSArray *)getSortedForDic:(NSMutableDictionary *)myDict {
    NSArray *myArray;
    myArray = [myDict keysSortedByValueUsingComparator: ^(id obj1, id obj2) {
        if ([obj1 integerValue] > [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        if ([obj1 integerValue] < [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
    return myArray;
}

- (void)initTextsMethod {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSData *data = [NSJSONSerialization dataWithJSONObject:@[@"Здравзтвуй, Родина моя! О, как любил тебя, как долго ждал я этой взтречи...", @"Здравствуй, Родина моя! О, как любил тебя, как долго жaждел я этой встречи..."] options:NSJSONWritingPrettyPrinted error:nil];
    NSString *json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSDictionary *parameters = @{@"text": json};
    [manager POST:@"https://speller.yandex.net/services/spellservice.json/checkTexts" parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Response: %@", responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

@end
