//
//  ProfileTableViewCell.h
//  Speller Test
//
//  Created by Alexander on 09.10.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TestYourselfViewController.h"

@interface ProfileTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userAvatar;
@property (weak, nonatomic) IBOutlet UIButton *testButton;
@property (weak, nonatomic) IBOutlet UIButton *testFriendsButton;
@property (weak, nonatomic) IBOutlet UILabel *cheerLabel;
@property (retain, nonatomic) UIView *statisticView;
@property (weak, nonatomic) TestYourselfViewController *parentVc;

- (void)loadUserAvatar;
- (CGFloat)getCurrentCellHeight;

@end
