//
//  FriendTableViewCell.h
//  Speller Test
//
//  Created by Alexander on 08.10.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userAvatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIView *userStatisticsView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userStatisticsViewWidthConstraint;
@property (nonatomic) CGFloat userStatistics;

- (void)loadUser:(NSDictionary *)user;

@end
