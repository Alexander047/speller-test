//
//  SpellerResponseParser.m
//  Speller Test
//
//  Created by Alexander on 06.10.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import "SpellerResponseParser.h"

@implementation SpellerResponseParser

+ (NSArray<SpellerResponse *> *)parsedResponse:(NSArray *)response {
    NSMutableArray <SpellerResponse *> *array = [NSMutableArray new];
    for (NSDictionary *dic in response) {
        SpellerResponse *responseObj = [SpellerResponse new];
        responseObj.code = [dic[@"code"] integerValue];
        responseObj.row = [dic[@"row"] integerValue];
        responseObj.column = [dic[@"col"] integerValue];
        responseObj.position = [dic[@"pos"] integerValue];
        responseObj.length = [dic[@"len"] integerValue];
        responseObj.suggestions = (NSArray *)dic[@"s"];
        responseObj.word = (NSString *)dic[@"word"];
        [array addObject:responseObj];
    }
    NSArray <SpellerResponse *> *parsedArray = [NSArray arrayWithArray:array];
    array = nil;
    return parsedArray;
}

@end
