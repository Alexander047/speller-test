//
//  FriendTableViewCell.m
//  Speller Test
//
//  Created by Alexander on 08.10.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import "FriendTableViewCell.h"

@implementation FriendTableViewCell

- (void)awakeFromNib {
    [self initGui];
}

- (void)initGui {
    self.userAvatarImageView.layer.cornerRadius = 22.0f;
    self.userAvatarImageView.layer.masksToBounds = YES;
}

- (void)loadUser:(NSDictionary *)user {
    self.userNameLabel.text = [NSString stringWithFormat:@"%@ %@", user[@"first_name"], user[@"last_name"]];
    self.userStatisticsViewWidthConstraint.constant = (self.userStatistics * (self.contentView.frame.size.width - 86.0f));
    self.userStatisticsView.backgroundColor = [UIColor colorWithRed:1.0f - self.userStatistics green:self.userStatistics blue:0 alpha:1.0f];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:user[@"photo_100"]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:15];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if (data) {
            self.userAvatarImageView.image = [UIImage imageWithData:data];
        }
        else NSLog(@"error while pulling image");
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
