//
//  SpellerResponseParser.h
//  Speller Test
//
//  Created by Alexander on 06.10.15.
//  Copyright © 2015 Alexander. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SpellerResponse.h"

@interface SpellerResponseParser : NSObject

+ (NSArray <SpellerResponse *> *)parsedResponse:(NSArray *)response;

@end
